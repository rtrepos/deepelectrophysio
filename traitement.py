import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt


def shuffle_samples(array, labels, cat_data=None):
    """
    Renvoie une permutation aleatoire des echantillons et labels associes, ainsi
    que les donnees categorielles (eventuellement) associees
    """
    permutation = np.arange(np.size(labels))
    np.random.shuffle(permutation)

    if cat_data is not None:
        return array[permutation,], labels[permutation,], cat_data[permutation,]
    
    return array[permutation,], labels[permutation,], None


def data_split(sequences, labels, training_split, cat_data=None):
    """
    Permet d'obtenir les train et test sets a partir des sequences generees,
    en garantissant la meme proportion de chaque label dans les deux sets.
    training_split: fraction des donnees a prendre dans le training set.
    """
    sequences, labels, cat_data = shuffle_samples(sequences, labels, cat_data)
        
    classes = np.unique(labels)
    first = True

    for cl in classes:
        sequences_cl = sequences[labels == cl,]
        size_cl = np.size(sequences_cl, 0)
        idx = int(size_cl * training_split)

        if cat_data is not None:
            cat_cl = cat_data[labels == cl,]

        if first:
            first = False
            
            train_x = sequences_cl[:idx,]
            test_x = sequences_cl[idx:,]
            
            train_y = cl * np.ones((idx,))
            test_y = cl * np.ones((size_cl - idx,))

            if cat_data is not None:
                train_cat = cat_cl[:idx,]
                test_cat = cat_cl[idx:,]
            else:
                train_cat, test_cat = None, None

        else:
            train_x = np.concatenate((train_x, sequences_cl[:idx,]), 0)
            test_x = np.concatenate((test_x, sequences_cl[idx:,]), 0)

            train_y = np.concatenate((train_y, cl * np.ones((idx,))))
            test_y = np.concatenate((test_y, cl * np.ones((size_cl - idx,))))

            if cat_data is not None:
                train_cat = np.concatenate((train_cat, cat_cl[:idx,]), 0)
                test_cat = np.concatenate((test_cat, cat_cl[idx:,]), 0)
    
    return train_x, train_y, test_x, test_y, train_cat, test_cat


def sample_standardization(train_x, test_x, val_x=None):
    """
    Regularisation centree reduite par rapport aux echantillons du training set
    """
    mean_train = np.mean(train_x, axis=0)
    std_train = np.std(train_x, axis=0)
    train_x = (train_x - mean_train) / std_train
    test_x = (test_x - mean_train) / std_train
    if val_x is not None:
        val_x = (val_x - mean_train) / std_train
    return train_x, test_x, val_x


def individual_standardization(array):
    """
    Regularisation (temporelle) centree reduite de chaque echantillon.
    """
    n, _, m = array.shape
    mean_x = np.mean(array, 1).reshape((n, m, 1))
    std_x = np.std(array, 1).reshape((n, m, 1))
    new_array = (array.transpose(0, 2, 1) - mean_x)
    new_array /= std_x
    return new_array.transpose(0, 2, 1)


def conversion_tensor(set_x, set_y, num_classes=None):
    """
    Retourne les arguments passes en tenseurs Tensorflow et les labels avec un
    one-hot encoding; num_classes est un argument optionnel donnant le nombre
    total de labels possibles (si c'est None, il est infere comme max(set_y)+1).
    """
    set_x = tf.convert_to_tensor(set_x)

    # on ajoute une dimension si les donnees sont 2D (de (n,m) a (n,m,1))
    if tf.rank(set_x) == 2:
        set_x = tf.expand_dims(set_x, 2)

    set_y = tf.keras.utils.to_categorical(set_y, num_classes=num_classes)

    set_y = tf.convert_to_tensor(set_y)
    return set_x, set_y


def precision_recall(conf):
    """
    Renvoie la precision et le rappel de chaque classe, a partir de la matrice
    de confusion (ou les lignes representent les classes veridiques et les
    colonnes les classes predites). 
    """
    precision = np.diag(conf) / np.sum(conf, 0)
    recall = np.diag(conf) / np.sum(conf, 1)
    return precision, recall


def balanced_accuracy(confusion_matrix):
    """
    Renvoie la veritable accuracy du modele, moyenne du taux de vrais positifs
    pour chaque classe.
    """
    _, recall = precision_recall(confusion_matrix)
    return np.mean(recall)


def selection_electrodes(sequences, electrodes, el_subset=None):
    """
    Renvoie les sequences ou l'ordre des electrodes a ete choisi aleatoirement;
    electrodes doit etre le nombre d'electrodes que l'on souhaite conserver
    pour les nouveaux echantillons; el_subset, si specifie, doit etre une liste
    d'entiers designant un sous-groupe d'electrodes au sein duquel on veut
    se limiter lors de l'elaboration des nouveaux echantillons. 
    """
    N, window, Nelectrodes = sequences.shape
    new_sequences = np.zeros((N, window, electrodes))

    if el_subset:
        perm = np.asarray(el_subset)
    else:
        # si el_subset est None, on utilise toutes les electrodes disponibles
        perm = np.arange(Nelectrodes)
        
    for i in range(np.size(sequences, 0)):
        np.random.shuffle(perm) # permutation aleatoire des electrodes choisies
        new_sequences[i,:,:] = sequences[i,:,perm[:electrodes]].T

    return new_sequences


def rearrange_electrodes(sequences, labels, el, groups=None):
    """
    Renvoie une nouvelle liste de sequences et de labels.
    el est une liste d'entiers designant chaque electrode de sequences que l'on
    souhaite conserver; groups doit etre une liste de listes d'electrodes, de
    facon a realiser une partition de el. Cette liste sera alors utilisee pour
    regrouper les echantillons selon chaque electrode. 
    """
    if groups is not None:
        first = True
        
        for group in groups:
            if first:
                seqs = sequences[:,:,group]
                first = False
            else:
                seqs = np.concatenate((seqs, sequences[:,:,group]), 0)
                
        labs = np.tile(labels, len(groups))
        return seqs, labs

    return sequences[:,:,el], labels


def save_loss_acc_plots(hist):
    """
    A partir d'un objet history retourne par la methode fit, affiche et
    enregistre l'evolution de la loss et de l'accuracy en fonction des epochs.
    """
    plt.plot(hist.history['loss'])
    plt.plot(hist.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper right')
    plt.savefig('loss.png')
    plt.clf()
    
    plt.plot(hist.history['accuracy'])
    plt.plot(hist.history['val_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.savefig('accuracy.png')
    plt.clf()




