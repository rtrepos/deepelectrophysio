import numpy as np
import pandas as pd
import tensorflow as tf
import os
import argparse

from custommodels import LuTrieschCNN, SimpleLSTM, MixedLSTM
from lecture import *
from donnees_son import *
from traitement import *


def attribute_plants(sequences, plant_el, train_plants, test_plants):
    """
    Renvoie les sequences d'entrainement et de test, par plante specifiee. 
    plant_el est la liste des plantes (e.g ['a1', 'a2', 't1']) associee a chaque
    electrode du tableau sequences; train_plants et test_plants sont des listes
    de plantes au meme format que plant_el, designant quelles plantes doivent
    servir respectivement a l'entrainement ou au test.
    """
    plants_el = np.asarray(plant_el)
    plants = np.unique(plants_el)
    train_el = np.zeros((len(plant_el),)).astype(bool)
    test_el = np.zeros((len(plant_el),)).astype(bool)
    for pl in train_plants:
        train_el += plants_el == pl
    for pl in test_plants:
        test_el += plants_el == pl
    return sequences[:,:,train_el], sequences[:,:,test_el]


def get_plant_labels(sequences, el_plant_labels):
    """
    Renvoie les labels de la plante associee a chaque sequence donnee, a l'aide
    de la liste el_plant_labels qui contient le label de la plante associee a
    chacune des electrodes lors d'un enregistrement donne.
    """
    nL = np.size(sequences, 0)
    lab = np.zeros((nL,))
    N = int(nL / len(el_plant_labels))
    for i in range(len(el_plant_labels)):
        if el_plant_labels[i][0] == 't':
            lab[(N*i):(N*(i+1)),] = 1.
    return lab


def get_plant_place_cat(sequences, el_plant_labels, el_place_labels):
    """
    Renvoie la representation One-hot des donnees categorielles pour les
    sequences temporelles associees; el_plant_labels contient le nom des plantes
    pour chaque electrode et el_place_labels le nom des positions de celles-ci.
    """
    plant_b = [int(p[0] == 't') for p in el_plant_labels]
    place_b = [int(p[0] == 'p') for p in el_place_labels]
    plan = tf.keras.utils.to_categorical(plant_b, 2).T
    plac = tf.keras.utils.to_categorical(place_b, 2).T
    cat_data = np.concatenate((plan, plac), 0)
    cat = np.asarray([cat_data for _ in range(np.size(sequences, 0))])
    return cat


def main():
    physical_devices = tf.config.list_physical_devices('GPU')
    if physical_devices:
        tf.config.experimental.set_memory_growth(physical_devices[0],
                                                 enable=True)
        
    desc = "Expérience de classification pour l'intégralité du son."
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-w', '--window', type=int, default=300,
                        help='longueur (nombre de points) de chaque sequence')
    parser.add_argument('-ep', '--epochs', type=int, default=15,
                        help="nombre de passes sur les données d'entraînement")
    parser.add_argument('-cp', '--classification_plantes',
                        action='count', default=0,
                        help='pour choisir de classifier selon les plantes')
    parser.add_argument('-pp', '--plant_place', action='count', default=0,
                        help="""pour choisir d'ajouter les plantes et les \
positions de chaque électrode en entrée""")
    parser.add_argument('-md', '--model', choices=['lstm', 'cnn', 'mixed'],
                        default='lstm', help="""choix du modèle: soit un LSTM, \
soit un CNN, soit un modèle à deux entrées pour gérer des données catégorielles\
supplémentaires""")
    parser.add_argument('-el', '--electrodes', type=int, choices=range(1,10),
                        default=9, help="nombre d'électrodes utilisées")
    args = parser.parse_args()

    ep = args.epochs
    window = args.window
    # parametres principaux des couches du reseau de neurone (cf custommodels)
    LSTMunits, denseunits = 100, 128

    # les donnees posterieures a 4h apres le son ne seront pas traitees
    cutoff = pd.Timedelta(hours=4) 
    
    classifier_son = args.classification_plantes == 0 # sinon, classif plantes
    plant_place = args.plant_place > 0 # sinon on utilise l'heure
    chosen_model = args.model
    electrodes = args.electrodes

    contains = ['Son']
    # fichiers inexploitables (mauvaise frequence d'echantillonnage, trop court)
    excludes = ['175237', '075956']
    files = get_TDMS_files('Manips', contains, excludes)

    first = True
    for filename in files:
        idx = int(filename[10]) - 1
        # obtention sequences et labels pour fichier courant filename
        label_ts = [sound_s[idx]]
        tr = (None, sound_s[idx] + cutoff)
        if classifier_son:
            seq, lab, cat = gather(filename, window, label_ts, hours=True,
                                  trunc=tr)
            seq = seq[:,:,:9]

            if plant_place:
                cat = get_plant_place_cat(seq,
                                          plant_el[idx][:9],
                                          place_el[idx][:9])
        else:
            seq, _, _ = gather(filename, window, label_ts, stack=False)
            lab = get_plant_labels(seq, plant_el[idx])
            cat = None

        # agregation des sequences et labels courants, pour chaque fichier    
        if first:
            seqs, labs = seq[:], lab[:]
            if cat is not None:
                cats = cat[:]
            else:
                cats = None
            first = False
        else:
            seqs = np.concatenate((seqs, seq[:]), 0)
            labs = np.concatenate((labs, lab[:]))
            if cat is not None:
                cats = np.concatenate((cats, cat[:]), 0)
        print(filename)

    output_classes = len(label_ts) + 1
    if not classifier_son:
        output_classes = 2

    if classifier_son:
        # ordre aleatoire des electrodes, fait par defaut
        seqs = selection_electrodes(seqs, electrodes)

    # creation des ensembles d'apprentissage
    train_x, train_y, vt_x, vt_y, train_cat, vt_cat = data_split(seqs, labs,
                                                                 .7, cats)
    test_x, test_y, val_x, val_y, test_cat, val_cat = data_split(vt_x, vt_y,
                                                                 .5, vt_cat)

    # regularisation usuelle
    train_x, test_x, val_x = sample_standardization(train_x, test_x, val_x)

    # conversion en tenseurs
    train_x, train_y, = conversion_tensor(train_x, train_y)
    test_x, test_y, = conversion_tensor(test_x, test_y)
    val_x, val_y, = conversion_tensor(val_x, val_y)
    
    if chosen_model == 'lstm': 
        model = SimpleLSTM(LSTMunits, denseunits, output_classes)
    elif chosen_model == 'cnn':
        model = LuTrieschCNN((window, electrodes), output_classes)
    else:
        model = MixedLSTM((window, electrodes), train_cat.shape[1:],
                          LSTMunits, denseunits, output_classes)

    model.compile(loss='categorical_crossentropy', optimizer='adam',
                  metrics=['accuracy'])

    if chosen_model != 'mixed':
        history = model.fit(train_x, train_y, epochs=ep, batch_size=128,
                            validation_data=(val_x, val_y), verbose=2)
        val_loss, val_acc = model.evaluate(val_x, val_y, verbose=2)
        predicted = np.argmax(model.predict(val_x), 1)
        
    else:
        history = model.fit({'seqs':train_x, 'cats':train_cat},
                            train_y, epochs=ep, batch_size=128,
                            validation_data=({'seqs':val_x, 'cats':val_cat},
                                             val_y),
                            verbose=2)
        val_loss, val_acc = model.evaluate({'seqs':val_x, 'cats':val_cat},
                                           val_y, verbose=2)
        predicted = np.argmax(model.predict({'seqs':val_x, 'cats':val_cat}), 1)

    conf = tf.math.confusion_matrix(np.argmax(val_y, 1), predicted)
    print(conf)
    print(balanced_accuracy(conf))
    precision, recall = precision_recall(conf)
    print("Precision: ", precision)
    print("Recall: ", recall)

    save_loss_acc_plots(history)


if __name__ == '__main__':
    main()

