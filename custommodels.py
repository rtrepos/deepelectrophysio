import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import models, layers


class LuTrieschCNN(models.Sequential):
    """
    Adapte de https://arxiv.org/abs/1903.08100
    """
    def __init__(self, input_shape, classes):
        super().__init__()

        # Block 1
        self.add(layers.Conv1D(filters=8,
                               kernel_size=9,
                               input_shape=input_shape))
        self.add(layers.Conv1D(filters=8,
                               kernel_size=9))
        self.add(layers.BatchNormalization())
        self.add(layers.LeakyReLU())
        self.add(layers.MaxPool1D(pool_size=4, strides=4))
        self.add(layers.Dropout(rate=0.2))

        # Block 2
        self.add(layers.Conv1D(filters=16,
                               kernel_size=9))
        self.add(layers.Conv1D(filters=16,
                               kernel_size=9))
        self.add(layers.BatchNormalization())
        self.add(layers.LeakyReLU())
        self.add(layers.MaxPool1D(pool_size=4, strides=4))
        self.add(layers.Dropout(rate=0.2))

        self.add(layers.Conv1D(filters=32,
                               kernel_size=9))
        self.add(layers.BatchNormalization())
        self.add(layers.LeakyReLU())
        self.add(layers.MaxPool1D(pool_size=4, strides=4))
        self.add(layers.Dropout(rate=0.2))

        self.add(layers.Flatten())
        self.add(layers.Dense(200))
        self.add(layers.LeakyReLU())
        self.add(layers.Dropout(rate=0.2))

        self.add(layers.Dense(classes, activation='softmax'))


class SimpleLSTM(models.Sequential):
    def __init__(self, LSTMunits, denseunits, classes):
        super().__init__()
        
        self.add(layers.LSTM(LSTMunits))
        self.add(layers.Dense(denseunits, activation = 'relu'))

        # derniere couche
        self.add(layers.Dense(classes, activation = 'softmax'))


class MixedLSTM(keras.Model):
    def __init__(self, seq_shape, cat_shape, LSTMunits, denseunits, classes):
        seq = keras.Input(shape=seq_shape, name='seqs')
        lstm_out = layers.LSTM(LSTMunits)(seq)
        
        cat = keras.Input(shape=cat_shape, name='cats')
        dense_cat = layers.Dense(8)(cat)
        if len(seq_shape) == len(cat_shape):
            dense_cat = layers.Flatten()(dense_cat)

        conc = layers.concatenate([lstm_out, dense_cat])
        out = layers.Dense(denseunits, activation='relu')(conc)
        out = layers.Dense(classes, activation='softmax', name='out')(out)

        super().__init__(inputs=[seq, cat], outputs=[out])


class RegressionLSTM(models.Sequential):
    def __init__(self, norm, LSTMunits, denseunits):
        super().__init__([norm,
                          layers.LSTM(LSTMunits),
                          layers.Dense(denseunits, activation='relu'),
                          layers.Dense(1)])

        



        
