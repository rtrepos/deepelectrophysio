"""
Donnees brutes associees a chaque experience d'infection dans Manips, au nombre
de cinq (d'ou la taille de chacune des listes). None indique une donnee non
disponible pour l'experience concernee. 
"""

import pandas as pd

# Pour chaque experience, etat de la plante associee a chacune des electrodes:
# t pour temoin, i pour infectee
infecte = [['t', 't', 't', 'i', 'i', 'i', 'i', 'i', 'i'],
           ['i', 'i', 'i', 't', 't', 't', 'i', 'i', 'i'],
           ['t', 't', 't', 'i', 'i', 'i', 'i', 'i', 'i'],
           ['i', 'i', 'i', 'i', 'i', 'i', 't', 't', 't'],
           ['i', 'i', 't', 't', 'i', 'i']]

# Date et heure du debut de l'infection, i.e de l'inoculation du champignon
infection = [pd.Timestamp(2019, 5, 14, 13, 51, 0),
             pd.Timestamp(2019, 5, 16, 8, 20, 0),
             pd.Timestamp(2019, 5, 20, 11, 40, 0),
             pd.Timestamp(2019, 5, 23, 9, 0, 0),
             pd.Timestamp(2019, 6, 4, 9, 10, 0)]

# Date et heure (approximatives) ou le debut de l'infection a ete visuellement
# constate par les personnes ayant conduit l'experience.
# Attention: donnees peu precises ou fiables
demarrage = [None, # l'infection n'a jamais pris
             pd.Timestamp(2019, 5, 17, 6, 0, 0),
             pd.Timestamp(2019, 5, 21, 11, 40, 0),
             pd.Timestamp(2019, 5, 25, 12, 0, 0),
             pd.Timestamp(2019, 6, 6, 9, 0, 0)]

# Liste des electrodes correspondant aux feuilles pour lesquelles on a releve la
# taille de la tache de necrose sur la feuille (cf necrose.py et le fichier
# excel "Manips/depart infection.xlsx"). 
necrose_electrode = [None,
                     [[0, 1, 2], [6, 7, 8]],
                     [[3, 4, 5], [6, 7, 8]],
                     None,
                     None]

# Coefficients obtenus pour chaque feuille dans la regression lineaire du log
# de la taille de la tache de la necrose par rapport au temps (en minutes)
# depuis le debut de l'infection, recuperes a partir du fichier excel deja cite.
# Les coeffiecients sont dans l'ordre [slope, intercept]. 
necrose_coeff = [None,
                 [[0.0009694, -1.8405], [0.0006544, -1.5027]],
                 [[0.0017024, -5.4271], [0.0017857, -5.2507]],
                 None,
                 None]
            
