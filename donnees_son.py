"""
Donnees brutes associees a chaque experience de son dans Manips, au nombre
de cinq (d'ou la taille de chacune des listes). None indique une donnee non
disponible pour l'experience concernee. 
"""

import pandas as pd


# Date et heure du debut de l'exposition au son pour chaque experience.
sound_s = [pd.Timestamp(2019, 6, 5, 17, 12, 0),
           pd.Timestamp(2019, 6, 7, 11, 50, 0),
           pd.Timestamp(2019, 6, 18, 16, 29, 0),
           pd.Timestamp(2019, 6, 20, 12, 20, 0),
           pd.Timestamp(2019, 6, 21, 15, 12, 0)]

# Duree (minutes) de l'exposition au son pour chaque experience. 
sound_d = [pd.Timedelta(minutes = 30),
           pd.Timedelta(minutes = 30),
           pd.Timedelta(minutes = 10),
           pd.Timedelta(minutes = 10),
           pd.Timedelta(minutes = 11)]

# Pour chaque experience, nom de la plante associee a chaque electrode:
# 'a' pour 'arabette', 't' pour 'tabac'; les numeros distinguent deux plantes
# dans une experience donnee (attention toutefois, 'a1' ne designe a priori pas
# du tout une meme plante pour Son1 et Son2). 
plant_el = [['a1', 'a1', 'a1', 't1', 't1', 'a2', 'a2', 'a2', 't2', 't2', 't2'],
            ['a1', 't2', 't2', 'a1', 'a1', 'a2', 'a2', 't1', 't1'],
            ['a1', 'a1', 'a1', 'a2', 'a2', 'a3', 'a3', 'a4', 'a4', 'a4'],
            ['a1', 'a1', 'a1', 'a2', 'a2', 'a2', 'a3', 'a3', 'a4', 'a4'],
            ['a1', 'a1', 'a1', 'a2', 'a2', 'a2', 'a3', 'a3', 'a4', 'a4']]

# Pour chaque experience, position de chaque electrode sur sa plante:
# 'p' pour electrode placee sur le petiole, 'b' au bas d'une feuille; les
# numeros n'ont de sens que pour distinguer deux electrodes d'une meme plante,
# identifiable grace a la liste precedente. 
place_el = [['p1', 'b1', 'b2', 'p1', 'b1', 'b1', 'p1', 'b2', 'p1', 'p2', 'b2'],
            ['b1', 'b1', 'p1', 'b2', 'p2', 'b1', 'p1', 'b1', 'p1'],
            ['b2', 'b1', 'b3', 'p1', 'm1', 'm1', 'p1', 'p1', 'm2', 'm1'],
            ['b1', 'b2', 'b3', 'b1', 'p1', 'b2', 'b1', 'p1', 'p1', 'b1'],
            ['b1', 'b2', 'b3', 'b1', 'p1', 'b2', 'b1', 'p1', 'p1', 'b1']]   
