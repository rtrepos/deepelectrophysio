from math import ceil
import numpy as np
import pandas as pd
import os
import argparse
from nptdms import TdmsFile
from pathlib import Path
from sklearn.metrics import confusion_matrix
import tensorflow as tf

from custommodels import SimpleLSTM
from traitement import *
from donnees_son import *


def get_TDMS_files(root_dir, contains, excludes):
    """
    Renvoie un iterable contenant tous les fichiers TDMS contenus dans le
    repertoire racine root_dir, tel que le chemin + nom du fichier:
        - contient une chaine de caracteres issue de la liste contains.
        - ne contient pas une chaine de caracteres issue de la liste excludes.
    """
    tdms_files = []
    # liste de tous les fichiers '.tdms' contenus dans root_dir
    for root, dirs, files in os.walk(root_dir):
        for file in files:
            if file.endswith('.tdms'):
                tdms_files.append(os.path.join(root, file))

    # on filtre selon contains et excludes
    if len(contains) == 0:
        contains = [''] # permet de prendre tous les fichiers
        
    def keep_file(filename):
        keep = False
        for x in contains:
            if x in filename:
                keep = True
                break
        for x in excludes:
            if x in filename:
                keep = False
                break
        return keep
    
    return sorted(filter(keep_file, tdms_files))


def get_data(path, display=False):
    """
    Renvoie (dans cet ordre) les donnees brutes associees a un fichier TDMS,
    l'heure de debut d'enregistrement et la frequence d'echantillonnage.
    Les donnees brutes sont un tableau numpy de forme (Nelectrodes, L) ou L est
    le nombre total de valeurs enregistrees pour chaque electrode. 
    Si display vaut True, affiche l'ordre dans lequel les electrodes ont ete
    traitees. 
    """
    tension = TdmsFile.read(path)
    data = tension[tension.groups()[0].name]
    ai = data[data.channels()[0].name]
    L = np.size(ai[:])
    fs = ai.properties['wf_samples']

    # UTC+2, heure d'ete France
    t0 = pd.Timestamp(ai.properties['DateTime']) + pd.Timedelta(hours=2)

    raw_data = np.zeros((len(data.channels()), L))
    for idx, electrode in enumerate(data.channels()):
        raw_data[idx,:] = data[electrode.name][:]

    if display:
        print([electrode.name for electrode in data.channels()])
        
    return raw_data, t0, fs


def truncate(raw_data, t0, fs, t1=None, t2=None):
    """
    A partir des donnees pre-traitees par get_data, de l'heure de debut et de
    la periode d'enchantillonnage, renvoie les donnees tronquees a la plage
    temporelle entre t1 et t2 (omettre t1 pour conserver toutes les valeurs
    jusqu'a t2, t2 pour conserver toutes celles apres t1).
    """
    if t1:
        t1 = int((t1 - t0).total_seconds()) * fs
        # on s'assure que l'index est positif
        t1 = max(0, t1)
        
    if t2:
        t2 = int((t2 - t0).total_seconds()) * fs
        t2 = max(0, t2)

    return raw_data[:,t1:t2]


def decoupage(array, window):
    """
    Decoupe le tableau temporel 1D array en blocs de sequences de taille window;
    dans le cas ou la longueur du tableau n'est pas divisible par window,
    par defaut on cree une derniere sequence en prenant les window dernieres
    valeurs. Le tableau retourne est de forme (N_sequences, window). 
    """
    L = np.size(array)
    sequences = [array[i:(i+window),] for i in range(0, L, window)]

    # derniere fenetre trop courte
    if L % window:
        sequences[-1] = array[-window:,]

    return np.asarray(sequences)


def get_labels(time_steps, window, t0, fs, label_timestamps):
    """
    Renvoie les labels pour des sequences de taille window issues d'un
    enregistrement de taille totale time_steps, demarrant a l'instant t0 et
    avec une frequence d'echantillonnage fs. label_timestamps doit etre la liste
    triee des instants correspondants a un nouveau label; par exemple si cette
    liste est [t1, t2], les labels seront 0 entre t0 et t1, 1 entre t1 et t2,
    et 2 apres t2.
    """
    N = ceil(time_steps / window) # nombre de sequences
    labels = np.zeros((N,))

    # conversion des instants en index d'array numpy (t0 correspondant a 0). 
    labels_idx = map(lambda t: max(int((t-t0).total_seconds()*fs/window), 0),
                     label_timestamps)

    cur_idx = 0
    for idx, label_idx in enumerate(labels_idx):
        labels[cur_idx:label_idx] = float(idx)
        cur_idx = label_idx
    labels[cur_idx:] = float(idx + 1)

    return labels


def get_sequences(raw_data, window, stack):
    """
    Renvoie les sequences de taille window associees a l'ensemble des donnees
    d'une experience; si stack vaut True les electrodes sont prises ensemble
    pour chaque echantillon, sinon chaque echantillon correspond a une unique
    electrode.
    Le tableau d'entree raw_data est de forme (Nelectrodes, L) (cf get_data),
    et celui de sortie (Nsequences, window, Nelectrodes) si stack vaut True,
    (Nsequences * Nelectrodes, window) sinon; ou Nsequences = ceil(L / window).
    """
    electrodes, L = raw_data.shape
    if not L:
        # fichier vide, aucune sequence a decouper
        return None

    sequences = [decoupage(raw_data[el,:], window) for el in range(electrodes)]   

    if not stack:
        return np.asarray(sequences).reshape(-1, window)

    return np.asarray(sequences).transpose(1, 2, 0)


def get_hours(time_steps, window, t0, fs):
    """
    Renvoie l'heure associee a chaque echantillon  de taille window issu d'un
    enregistrement de taille time_steps, demarrant a t0, avec fs points par
    seconde. L'heure est encodee comme un doublet (sin, cos) pour conserver la
    notion que 23:30 est proche de 0:30.
    """
    N = ceil(time_steps / window)
    seq_len = pd.Timedelta(seconds=window / fs)
    
    time = [t0 + i * seq_len for i in range(N)]
    hours = np.asarray([x.hour + x.minute / 60 for x in time])
    
    hours_sin = np.sin(hours * (2 * np.pi / 24))
    hours_cos = np.cos(hours * (2 * np.pi / 24))

    return np.vstack((hours_sin, hours_cos)).T


def gather(path, window, label_ts, trunc=None, stack=True, hours=False):
    """
    A partir du chemin vers un fichier TDMS, renvoie les sequences de taille
    window associees, ainsi que les labels a partir de la liste label_ts,
    contenant les instants ou l'on change de label (cf get_labels). Si precise,
    trunc doit etre un tuple de deux instants (ou None) permettant de se
    focaliser sur une sous-partie de l'enregistrement.
    Par defaut les echantillons sont de la forme (Nseq, window, Nelectrodes),
    sauf si stack vaut False, auquel cas les electrodes sont melangees et les
    sequences de forme (Nseq * Nelectrodes, window). 
    Le booleen hours permet d'obtenir l'heure associee a chaque echantillon,
    sous la forme (sin, cos) (cf get_hours). 
    """
    raw_data, t0, fs = get_data(path, display=False)
    if trunc:
        raw_data = truncate(raw_data, t0, fs, *trunc)
        if trunc[0]:
            t0 = trunc[0]
            
    electrodes, L = raw_data.shape
    sequences = get_sequences(raw_data, window, stack)
    labels = get_labels(L, window, t0, fs, label_ts)

    if not stack:
        labels = np.tile(labels, electrodes)

    if hours:
        hr = get_hours(L, window, t0, fs)
        return sequences, labels, hr

    return sequences, labels, None
    
            
def main():
    physical_devices = tf.config.list_physical_devices('GPU')
    if physical_devices:
        tf.config.experimental.set_memory_growth(physical_devices[0],
                                                 enable=True)

    # parse des arguments en ligne de commande
    path = "Manips/Son1/05062019_153257/Tension_1.tdms"
    desc = 'Expérience de son où N électrodes servent à prédire les autres.'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-te', '--test_el', choices=range(1,11), type=int,
                        default=1, help="nombre d'électrodes de test")
    parser.add_argument('-w', '--window', type=int, default=1000,
                        help='longueur (nombre de points) de chaque sequence')
    parser.add_argument('-ep', '--epochs', type=int, default=10,
                        help="nombre de passes sur les données d'entraînement")
    args = parser.parse_args()

    # principaux hyperparametres
    path = Path(path) # marche pour tout systeme d'exploitation
    manip_idx = 0
    son_debut = sound_s[manip_idx]
    son_duree = sound_d[manip_idx]
    label_ts = [son_debut, son_debut + son_duree]
    window = args.window
    output_classes = len(label_ts) + 1
    test_el = args.test_el
    ep = args.epochs
    LSTMunits, denseunits = 100, 128

    # recuperation des donnees traitees
    seq, lab, _ = gather(path, window, label_ts, stack=False)
    
    # experience ou l'on isole une electrode
    N = np.size(seq, 0)
    N_electrodes = len(plant_el[manip_idx])
    nel = test_el * int(N / N_electrodes)
    train_x, train_y = seq[nel:,:], lab[nel:]
    test_x, test_y = seq[:nel,:], lab[:nel]
    train_x, test_x, _ = sample_standardization(train_x, test_x)
    train_x, train_y = conversion_tensor(train_x, train_y, output_classes)
    test_x, test_y = conversion_tensor(test_x, test_y, output_classes)

    model = SimpleLSTM(LSTMunits, denseunits, output_classes)
    model.compile(loss='categorical_crossentropy', optimizer='adam',
                  metrics=['accuracy'])
    history = model.fit(train_x, train_y, epochs=ep, batch_size=128, verbose=2,
                        validation_split=.2)
    test_loss, test_acc = model.evaluate(test_x, test_y, verbose=2)

    predicted = np.argmax(model.predict(test_x), 1)
    print(confusion_matrix(np.argmax(test_y, 1), predicted))

    save_loss_acc_plots(history)
    

if __name__ == '__main__':
    main()


