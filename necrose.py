import pandas as pd
import numpy as np
import tensorflow as tf
from tensorflow.keras import models, layers
import argparse

from donnees_infection import *
from traitement import *
from lecture import *
from custommodels import RegressionLSTM


def taille_necrose(path, window, electrodes, coeff, t_infection):
    """
    A partir du chemin vers un fichier TDMS, renvoie (dans cet ordre) les
    sequences de taille window associees, le temps ecoule (en minutes) entre le
    debut de l'enregistrement et le debut de chaque sequence (poss. negatif),
    et la log_10-valeur de la taille de la necrose a ce moment precis.
    electrodes doit etre une liste d'entiers designant les electrodes a
    conserver pour chaque sequence; coeff doit etre un tuple indiquant les
    coefficients (pente, ordonnee a l'origine) issus de la regression lineaire
    du log_10 de la taille de la necrose sur le temps ecoule en minutes depuis
    le debut de l'infection.
    t_infection indique le debut de l'infection. 
    """
    raw_data, t0, fs = get_data(path)
    
    sequences = get_sequences(raw_data, window, True)
    sequences = sequences[:,:,electrodes]
    
    N = np.size(sequences, 0)
    seq_len = pd.Timedelta(seconds=window/fs)
    time = [t0 + i * seq_len for i in range(N)] # debut de chaque sequence

    # temps ecoule depuis le debut de l'infection
    minutes = np.asarray([(t-t_infection).total_seconds()/60 for t in time])

    tailles = np.zeros(minutes.shape)
    # on calcule les valeurs uniquement apres le debut de l'infection
    tailles[minutes >= 0] = coeff[0] * minutes[minutes >= 0] + coeff[1]
    
    return sequences, minutes, tailles


def separation_apres_avant(sequences, minutes, tailles):
    """
    A partir des donnees deja traitees, les sequences decoupees, les minutes
    depuis entre le debut des sequences et le debut de l'infection (negatives si
    la sequence precede l'infection) et la log-taille de la necrose, on separe
    et renvoie deux tuples, les (sequences, minutes, tailles) commencant apres
    le debut de l'infection et les (sequences, minutes) commencant avant le
    debut de l'infection. Pour le second on omet les tailles car elles n'ont pas
    de sens, la taille de la necrose etant 0, la log-taille devrait etre -inf. 
    """
    avant = minutes < 0
    apres = np.logical_not(avant)
    seqs, mins, ts = sequences[apres], minutes[apres], tailles[apres]
    return (seqs, mins, ts), (sequences[avant], minutes[avant])


def main():
    physical_devices = tf.config.list_physical_devices('GPU')
    if physical_devices:
        tf.config.experimental.set_memory_growth(physical_devices[0],
                                                 enable=True)

    desc = """Régression de la taille de la nécrose par rapport aux minutes \
écoulées depuis le début de l'infection."""
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-m', '--manip', choices=range(2,4), type=int,
                        default=2, help='numéro de la manip à traiter')
    parser.add_argument('-p', '--plant', choices=range(0,2), type=int,
                        default=0, help='plante à considérer')
    parser.add_argument('-w', '--window', type=int, default=300,
                        help='longueur (nombre de points) de chaque sequence')
    parser.add_argument('-ep', '--epochs', type=int, default=15,
                        help="nombre de passes sur les données d'entraînement")
    args = parser.parse_args()
    
    manip = args.manip
    plant = args.plant
    el = necrose_electrode[manip-1][plant]
    coeff = necrose_coeff[manip-1][plant]

    window = args.window
    ep = args.epochs
    training_split = 0.7

    contains = ['Infection' + str(manip)]
    excludes = []
    files = get_TDMS_files('Manips', contains, excludes)

    # on recupere, traite et regroupe les donnees de chaque fichier
    first = True
    for filename in files:
        print(filename)
        idx = manip - 1

        tr = infection[idx]
        seq, minute, lab = taille_necrose(filename, window, el, coeff, tr)

        if first:
            seqs = seq[:]
            labs = lab[:]
            minutes = minute[:]
            first = False
        else:
            seqs = np.concatenate((seqs, seq[:]), 0)
            labs = np.concatenate((labs, lab[:]))
            minutes = np.concatenate((minutes, minute[:]))
    
    # on separe les donnees avant et apres le debut de l'infection
    apres, avant = separation_apres_avant(seqs, minutes, labs)
    seqs, minutes, labs = apres
    avant_seqs, avant_minutes = avant

    # on separe entre training et test sets
    seqs, labs, minutes = shuffle_samples(seqs, labs, minutes)
    split_idx = int(training_split * np.size(labs))
    train_x, train_y = seqs[:split_idx,], labs[:split_idx]
    test_x, test_y = seqs[split_idx:,], labs[split_idx:]
    test_minutes = minutes[split_idx:]

    # standardization
    normalizer = layers.experimental.preprocessing.Normalization()
    normalizer.adapt(train_x)

    # definition et entrainement du modele (20% du training set en validation)
    LSTMunits, denseunits = 100, 128
    model = RegressionLSTM(normalizer, LSTMunits, denseunits)
    model.compile(loss='mean_absolute_error', optimizer='adam')
    history = model.fit(train_x, train_y, epochs=ep, batch_size=128,
                        validation_split=0.2, verbose=2)

    # evaluation du modele sur les donnees de test
    val_loss = model.evaluate(test_x, test_y, verbose=2)
    test_pred = model.predict(test_x).reshape(-1)
    pre_pred = model.predict(avant_seqs).reshape(-1)

    # courbe d'apprentissage
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper right')
    plt.savefig('necrose_loss.png')
    plt.clf()

    # affichage des donnees de test et des predictions
    plt.scatter(test_minutes, test_y, label='True', s=1)
    plt.scatter(test_minutes, test_pred, label='Pred', s=1)
    
    # decommenter la ligne ci-dessous pour ajouter les predictions du modele
    # pour des donnees anterieures a l'infection
    # plt.scatter(avant_minutes, pre_pred, label='Pre-pred', s=1)
    
    plt.xlabel('Minutes since infection')
    plt.ylabel('Necrosis')
    plt.legend(loc='upper left')
    plt.savefig('necrose_scatter.png')
    plt.clf()

    # calcul du coeff de determination R^2
    # (predictions par rapport a la droite attendue)
    mean_true = np.mean(test_y)
    ss_tot = np.sum((test_y - mean_true)**2)
    ss_res = np.sum((test_y - test_pred)**2)
    r_squared = 1 - ss_res / ss_tot
    print('R^2:', r_squared)
    

if __name__ == '__main__':
    main()
    
