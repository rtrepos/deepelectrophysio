import pandas as pd
import numpy as np
import tensorflow as tf
import argparse

from donnees_infection import *
from traitement import *
from lecture import *
from custommodels import *


def random_test_sampling(data, t0, fs, label_ts, window, n_samples):
    """
    A partir des donnees d'un enregistrement non decoupe, demarre a t0 et avec
    fs points par seconde, renvoie n_samples sequences de taille window prises
    au hasard, ainsi que les labels associes. label_ts est la liste des instants
    marquant un changement de label.
    """
    electrodes, L = data.shape
    if not L:
        return None, None
    random_start = np.random.randint(L - window, size=n_samples)
    label_ts = np.asarray(label_ts)
    
    samples = np.zeros((n_samples, window, electrodes))
    labels = np.zeros((n_samples,))
    for idx, start in enumerate(random_start):
        samples[idx, :, :] = data[:,start:(start+window)].T
        t = t0 + pd.Timedelta(seconds=start/fs)

        # determination du label, 1er indice apres un element de label_ts
        labels[idx] = float(np.argmax(label_ts > t))
        if t >= label_ts[-1]:
            # cas ou l'echantillon  correspond au dernier label (argmax donne 0)
            labels[idx] = float(np.size(label_ts))
        
    return samples, labels


def random_from_path(files, label_ts, window, n_samples=None, trunc=None):
    """
    A partir d'une liste de chemins de fichiers TDMS et de la liste label_ts
    des instants marquant un changement de label, renvoie une portion aleatoire
    de sequences de taille window de cet enregistement ainsi que les labels
    associes.
    test_split determine la proportion de ces sequences dans l'enregistrement,
    et trunc est un tuple qu'on peut speficier pour tronquer une partie de
    l'enregistrement au prealable.
    """
    first = True
    for path in files:
        raw_data, t0, fs = get_data(path, display=False)
        if trunc:
            raw_data = truncate(raw_data, t0, fs, *trunc)
            if trunc[0] and t0 < trunc[0]:
                t0 = trunc[0]

        if not n_samples:
            n_samples = int(np.size(raw_data, 1) / window * .5)
        
        samples, labels = random_test_sampling(raw_data, t0, fs, label_ts,
                                               window, n_samples)
        if samples is None:
            continue

        if first:
            samps, labs = samples[:], labels[:]
            first = False
        else:
            samps = np.concatenate((samps, samples[:]), 0)
            labs = np.concatenate((labs, labels[:]))
            
    return samps, labs
            
       
    
def main():
    physical_devices = tf.config.list_physical_devices('GPU')
    if physical_devices:
        tf.config.experimental.set_memory_growth(physical_devices[0],
                                                 enable=True)

    desc = "Expérience de classification pour l'infection."
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-m', '--manip', choices=range(2,6), type=int,
                        default=2, help='numéro de la manip à traiter')
    parser.add_argument('-p', '--plant', choices=range(0,3), type=int,
                        default=0, help='plante à considérer')
    parser.add_argument('-w', '--window', type=int, default=300,
                        help='longueur (nombre de points) de chaque sequence')
    parser.add_argument('-ep', '--epochs', type=int, default=15,
                        help="nombre de passes sur les données d'entraînement")
    parser.add_argument('-re', '--random_eval', action='count', default=0,
                        help="""à indiquer si l'on souhaite évaluer le modèle \
sur une sélection aléatoire d'échantillons choisis dans une plage temporelle \
donnée; dans ce cas les ensembles de validation et de test sont confondus""")
    args = parser.parse_args()

    plant = args.plant
    manip = args.manip
    window = args.window
    ep = args.epochs
    random_eval = args.random_eval > 0
    idx = manip - 1
    LSTMunits, denseunits = 100, 128

    plus_4 = infection[idx] + pd.Timedelta(hours=4)
    plus_12 = infection[idx] + pd.Timedelta(hours=12)
    label_ts = [plus_4, plus_12]
    output_classes = len(label_ts) + 1
    # commenter la ligne suivante si l'on souhaite conserver les donnees avant
    # l'inoculation du champignon
    tr = (infection[idx], None)

    contains = ['Infection' + str(manip)]
    excludes = []
    root = 'Manips'
    files = get_TDMS_files(root, contains, excludes)

    first = True
    for filename in files:
        seq, lab, hr = gather(filename, window, label_ts, hours=True, trunc=tr)

        if seq is None:
            continue
        
        plant_el = int(np.size(seq, 2) / 3) # nombre d'electrodes par plante
        # liste des electrodes associees a la plante choisie
        el = list(range(plant * plant_el, (plant + 1) * plant_el))
        seq = seq[:,:,el]

        if first:
            seqs = seq[:]
            labs = lab[:]
            cats = hr[:]
            first = False
        else:
            seqs = np.concatenate((seqs, seq[:]), 0)
            labs = np.concatenate((labs, lab[:]))
            cats = np.concatenate((cats, hr[:]), 0)

        print(filename)

    if random_eval:
        # evaluation en prenant des sequences au hasard sur une certaine
        # plage temporaire
        test_files = get_TDMS_files(root, contains, excludes)
        test_bounds = [pd.Timedelta(hours=2), pd.Timedelta(hours=8)] 
        test_tr = (infection[idx]+test_bounds[0],infection[idx]+test_bounds[1])
        test_seq, test_lab = random_from_path(test_files, label_ts, window,
                                              trunc=test_tr, n_samples=1000)
        test_seq = test_seq[:,:,el]

        val_x, val_y, _ = shuffle_samples(test_seq, test_lab)
        test_x, test_y = val_x, val_y # validation et test confondus
        train_x, train_y, _ = shuffle_samples(seqs, labs)

    else:
        # separation (train, validation, test) classique des donnees
        train_x, train_y, vt_x, vt_y, train_cat, vt_cat = data_split(seqs, labs,
                                                                     .7, cats)
        test_x, test_y, val_x, val_y, test_cat, val_cat = data_split(vt_x, vt_y,
                                                                     .5, vt_cat)
        
    train_x, test_x, val_x = sample_standardization(train_x, test_x, val_x)

    # conversion en tenseurs
    train_x, train_y = conversion_tensor(train_x, train_y)
    test_x, test_y = conversion_tensor(test_x, test_y, output_classes)
    val_x, val_y = conversion_tensor(val_x, val_y, output_classes)

    model = SimpleLSTM(LSTMunits, denseunits, output_classes)
    model.compile(loss='categorical_crossentropy', optimizer='adam',
                  metrics=['accuracy'])

    history = model.fit(train_x, train_y, epochs=ep, batch_size=128,
                        validation_data=(val_x, val_y), verbose=2)
    val_loss, val_acc = model.evaluate(val_x, val_y, verbose=2)
    predicted = np.argmax(model.predict(val_x), 1)
    
    conf = tf.math.confusion_matrix(np.argmax(val_y, 1), predicted)
    print(conf)
    print(balanced_accuracy(conf))
    prec, rec = precision_recall(conf)
    print("Precision: ", prec)
    print("Recall:    ", rec)

    save_loss_acc_plots(history)



if __name__ == '__main__':
    main()
    
