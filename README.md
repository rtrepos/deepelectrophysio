# DeepElectroPhysio

Projet de traitement de données de type électrophysiologiques par des algorithmes de deep learning, par Léandre Genin au cours d'un stage à l'unité MIAT.  

## Données et pré-requis

Ce code a été écrit de façon à traiter des données regroupées dans un dossier (non inclus dans ce git) nommé _Manips_ situé au niveau du répertoire courant. Toute fonction **main** présuppose la présence de ce dossier, ainsi qu'une arborescence de type _Manips/InfectionX/.../.tdms_ où _X_ est un entier. 

Les seules expériences traitées sont celles liées au son et à l'infection. Se référer à Fred Garcia pour plus d'informations sur les données. 

### Dépendances Python 3

Le code nécessite les bibliothèques Python suivantes : 
* **tensorflow 2.0** (et **keras**)
* **numpy**
* **pandas**
* **nptdms**, uniquement pour la lecture des fichiers .TDMS

### Lancer un programme

Pour exécuter un programme de test, il suffit de lancer Python 3 sur l'un des fichiers comportant une fonction **main**, par exemple
```
python3 son.py
```

Chaque programme de test permet aussi de passer directement des arguments en ligne de commande; pour en savoir plus, s'en référer à l'option **-h**, comme ceci:
```
python3 infection.py -h
```

##  Description des fichiers

### Modules directement exécutables

On compte trois programmes conçus pour effectuer des tests:
* **son.py**, qui permet de définir, entraîner et évaluer divers modèles sur les données de _Manips_ liées aux epxériences avec du son;
* **infection.py**, qui permet la même chose pour les expériences d'infection par _Sclerotinia_ (à ceci près que chaque lancement ne porte que sur une seule plante et pour une seule expérience);
* **necrose.py**, qui permet, pour deux expériences d'infection, d'effectuer la régression de la taille de la tache (nécrose) sur la feuille par rapport aux séquences électrophysiologiques.

Ces programmes contiennent également les quelques fonctions spécifiques à leur sujet propre. 

Le programme **lecture.py** peut aussi être lancé pour effectuer une petite expérience de test sur du son (utiliser k électrodes pour l'entraînement du modèle, et prendre les autres pour l'évaluation), mais sa raison d'être est avant tout utilitaire. 

### Modules auxiliaires

Parmi les programmes auxiliaires, les fichiers **donnees_son.py** et **donnees_infection.py** servent uniquement à stocker des informations spécifiques aux expériences respectives de son et d'infection, informations normalement indiquée "à la main" dans les données. Ils sont sans autre intérêt algorithmique.

Le module **custommodels.py** contient quant à lui la définition des différents modèles de réseaux neuronaux que l'on peut utiliser pour le traitement de données, _i.e._ un CNN, un LSTM et un réseau mixte à double entrée (un LSTM et un petit réseau dense pour traiter des données catégorielles supplémentaires). 

Le module **lecture.py**, quoique exécutable, sert avant tout à la lecture des données issues de fichiers TDMS, jusqu'à leur découpage en séquences de taille fixe. 

Le module **traitement.py**, enfin, est utilitaire et contient les fonctions générales de traitement des données une fois extraites (_e.g._ séparation _train-test_, standardisation...). 

